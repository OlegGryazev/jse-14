package ru.gryazev.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.endpoint.ITaskEndpoint;
import ru.gryazev.tm.endpoint.Task;
import ru.gryazev.tm.error.CrudNotFoundException;
import ru.gryazev.tm.error.CrudUpdateException;

@NoArgsConstructor
public final class TaskLinkCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "task-link";
    }

    @Override
    public String getDescription() {
        return "Link selected task to project.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) return;
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        @NotNull final String token = getToken();
        @Nullable final String currentProjectId = getCurrentProjectId();
        final int projectIndex = terminalService.getProjectIndex();
        @Nullable final String newProjectId = serviceLocator.getProjectEndpoint()
                .getProjectId(token, projectIndex);
        if (newProjectId == null) throw new CrudNotFoundException();
        final int taskIndex = terminalService.getTaskIndex();
        @Nullable final String taskId = taskEndpoint.getTaskId(token, currentProjectId, taskIndex);
        if (taskId == null) throw new CrudNotFoundException();
        @Nullable final Task task = taskEndpoint.findOneTask(token, taskId);
        if (task == null) throw new CrudNotFoundException();
        task.setProjectId(newProjectId);
        @Nullable final Task linkedTask = taskEndpoint
                .editTask(token, task);
        if (linkedTask == null) throw new CrudUpdateException();
        terminalService.print("[OK]");
    }

}
