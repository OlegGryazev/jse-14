package ru.gryazev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.repository.IRepository;
import ru.gryazev.tm.entity.AbstractCrudEntity;

import java.util.List;

public interface IService<T extends AbstractCrudEntity> {

    @Nullable
    public T edit(@Nullable String userId, @Nullable T t);

    @NotNull
    public IRepository<T> getRepository();

}
