package ru.gryazev.tm.api.context;

import org.jetbrains.annotations.Nullable;

public interface SessionLocator {

    public void setToken(@Nullable String token);

    @Nullable
    public String getToken();

}
