package ru.gryazev.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.dto.Session;
import ru.gryazev.tm.enumerated.RoleType;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "app_session")
public class SessionEntity extends AbstractCrudEntity {

    @NotNull
    @ManyToOne
    private UserEntity user;

    @Nullable
    @Enumerated(EnumType.STRING)
    private RoleType role;

    @Nullable
    @JsonIgnore
    private String signature;

    @NotNull
    private Long timestamp = new Date().getTime();

    @Nullable
    public static Session toSessionDto(@Nullable final SessionEntity sessionEntity) {
        if (sessionEntity == null) return null;
        @NotNull final Session session = new Session();
        session.setId(sessionEntity.getId());
        session.setSignature(sessionEntity.getSignature());
        session.setUserId(sessionEntity.getUser().getId());
        session.setRole(sessionEntity.getRole());
        session.setTimestamp(sessionEntity.getTimestamp());
        return session;
    }

}
