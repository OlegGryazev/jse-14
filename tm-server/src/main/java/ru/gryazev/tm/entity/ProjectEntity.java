package ru.gryazev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.dto.Project;
import ru.gryazev.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "app_project")
public class ProjectEntity extends AbstractCrudEntity {

    @NotNull
    @ManyToOne
    private UserEntity user;

    @NotNull
    @OneToMany(mappedBy = "project", orphanRemoval = true)
    private List<TaskEntity> tasks;

    @Nullable
    private String name;

    @Nullable
    private String details;

    @Nullable
    private Date dateStart;

    @Nullable
    private Date dateFinish;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.PLANNED;

    @NotNull
    private Long createMillis = new Date().getTime();

    @Nullable
    public static Project toProjectDto(@Nullable final ProjectEntity projectEntity) {
        if (projectEntity == null) return null;
        @NotNull final Project project = new Project();
        project.setId(projectEntity.getId());
        project.setName(projectEntity.getName());
        project.setDetails(projectEntity.getDetails());
        project.setDateStart(projectEntity.getDateStart());
        project.setDateFinish(projectEntity.getDateFinish());
        project.setStatus(projectEntity.getStatus());
        project.setUserId(projectEntity.getUser().getId());
        project.setCreateMillis(projectEntity.getCreateMillis());
        return project;
    }

}
