package ru.gryazev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.dto.Task;
import ru.gryazev.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "app_task")
public class TaskEntity extends AbstractCrudEntity {

    @NotNull
    @ManyToOne
    private UserEntity user;

    @Nullable
    @ManyToOne
    private ProjectEntity project;

    @Nullable
    private String name;

    @Nullable
    private String details;

    @Nullable
    private Date dateStart;

    @Nullable
    private Date dateFinish;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.PLANNED;

    @NotNull
    private Long createMillis = new Date().getTime();

    @Nullable
    public static Task toTaskDto(@Nullable final TaskEntity taskEntity) {
        if (taskEntity == null) return null;
        @NotNull final Task task = new Task();
        task.setId(taskEntity.getId());
        task.setProjectId(taskEntity.getProject() == null ? null : taskEntity.getProject().getId());
        task.setName(taskEntity.getName());
        task.setDetails(taskEntity.getDetails());
        task.setDateStart(taskEntity.getDateStart());
        task.setDateFinish(taskEntity.getDateFinish());
        task.setStatus(taskEntity.getStatus());
        task.setUserId(taskEntity.getUser().getId());
        task.setCreateMillis(taskEntity.getCreateMillis());
        return task;
    }

}
