package ru.gryazev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.UUID;

@Setter
@Getter
@MappedSuperclass
public class AbstractCrudEntity {

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

}
