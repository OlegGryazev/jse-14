package ru.gryazev.tm;

import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.context.Bootstrap;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public final class Application {

    public static void main(final String[] args) throws Exception {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }

}
