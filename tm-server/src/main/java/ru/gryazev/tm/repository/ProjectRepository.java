package ru.gryazev.tm.repository;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.repository.IProjectRepository;
import ru.gryazev.tm.entity.ProjectEntity;

import javax.persistence.EntityManager;
import java.util.List;

@AllArgsConstructor
public class ProjectRepository implements IProjectRepository {

    @NotNull
    private final EntityManager entityManager;

    @Nullable
    public ProjectEntity findOneById(@NotNull final String userId, @NotNull final String id) {
        return entityManager.createQuery("SELECT p FROM ProjectEntity p WHERE user.id = :userId AND id = :id", ProjectEntity.class)
                .setParameter("userId", userId).setParameter("id", id).getSingleResult();
    }

    public void persist(@NotNull final ProjectEntity projectEntity) {
        entityManager.persist(projectEntity);
    }

    public void removeById(@NotNull final String userId, @NotNull final String id) {
        entityManager.remove(findOneById(userId, id));
    }

    public void removeAllByUserId(@NotNull final String userId) {
        for (final ProjectEntity projectEntity : findAllByUserId(userId)) entityManager.remove(projectEntity);
    }

    public void merge(@NotNull final ProjectEntity projectEntity) {
        entityManager.merge(projectEntity);
    }

    @NotNull
    public List<ProjectEntity> findAll() {
        return entityManager.createQuery("SELECT p FROM ProjectEntity p", ProjectEntity.class).getResultList();
    }

    public void removeAll() {
        for (final ProjectEntity projectEntity : findAll()) entityManager.remove(projectEntity);
    }

    @NotNull
    public List<ProjectEntity> findAllByName(@NotNull final String userId, @NotNull final String projectName) {
        return entityManager.createQuery("SELECT p FROM ProjectEntity p WHERE user.id = :userId AND name LIKE :projectName", ProjectEntity.class)
                .setParameter("userId", userId).setParameter("projectName", "%" + projectName + "%").getResultList();
    }

    @NotNull
    public List<ProjectEntity> findAllByDetails(@NotNull final String userId, @NotNull final String projectDetails) {
        return entityManager.createQuery("SELECT p FROM ProjectEntity p WHERE user.id = :userId AND details LIKE :projectDetails", ProjectEntity.class)
                .setParameter("userId", userId).setParameter("projectDetails", "%" + projectDetails + "%").getResultList();
    }

    @NotNull
    public List<ProjectEntity> findAllByUserId(@NotNull final String userId) {
        return entityManager.createQuery("SELECT p FROM ProjectEntity p WHERE user.id = :userId", ProjectEntity.class)
                .setParameter("userId", userId).getResultList();
    }

    @NotNull
    public List<ProjectEntity> findAllByUserIdSorted(@NotNull final String userId, @NotNull final String sqlSortType) {
        return entityManager.createQuery("SELECT p FROM ProjectEntity p WHERE user.id = :userId ORDER BY :sortType", ProjectEntity.class)
                .setParameter("userId", userId).setParameter("sortType", sqlSortType).getResultList();
    }

}
