package ru.gryazev.tm.repository;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.api.repository.ISessionRepository;
import ru.gryazev.tm.entity.SessionEntity;
import ru.gryazev.tm.entity.UserEntity;

import javax.persistence.EntityManager;
import java.util.List;

@AllArgsConstructor
public class SessionRepository implements ISessionRepository {

    @NotNull
    private final EntityManager entityManager;

    public SessionEntity findOneById(@NotNull final String id) {
        return entityManager.createQuery("SELECT s FROM SessionEntity s WHERE s.id = :id", SessionEntity.class)
                .setParameter("id", id).getSingleResult();
    }

    public void persist(@NotNull final SessionEntity sessionEntity) {
        entityManager.persist(sessionEntity);
    }

    public void removeById(@NotNull final String id) {
        entityManager.remove(findOneById(id));
    }

    public void removeAllByUserId(@NotNull final String userId){
        entityManager.createQuery("DELETE FROM SessionEntity s WHERE s.user.id = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void merge(@NotNull final SessionEntity sessionEntity) throws Exception {
        throw new Exception("Session edit is not allowed.");
    }

    @NotNull
    public List<SessionEntity> findAll() {
        return entityManager.createQuery("SELECT s FROM SessionEntity s", SessionEntity.class).getResultList();
    }

    @NotNull
    public List<SessionEntity> findByUserId(@NotNull final String userId) {
        return entityManager.createQuery("SELECT s FROM SessionEntity s " +
                "WHERE user.id = :userId", SessionEntity.class).setParameter("userId", userId).getResultList();
    }

    public void removeAll() {
        entityManager.createQuery("DELETE FROM SessionEntity").executeUpdate();
    }

}
