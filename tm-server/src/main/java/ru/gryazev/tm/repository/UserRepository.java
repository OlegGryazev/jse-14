package ru.gryazev.tm.repository;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.repository.IUserRepository;
import ru.gryazev.tm.entity.AbstractCrudEntity;
import ru.gryazev.tm.entity.UserEntity;

import javax.persistence.EntityManager;
import java.util.List;

@AllArgsConstructor
public class UserRepository implements IUserRepository {

    @NotNull
    private final EntityManager entityManager;

    @Nullable
    public UserEntity findOneById(@NotNull final String id) {
        return entityManager.createQuery("SELECT u FROM UserEntity u WHERE u.id = :id", UserEntity.class)
                .setParameter("id", id).getSingleResult();
    }

    public void persist(@NotNull final UserEntity userEntity) {
        entityManager.persist(userEntity);
    }

    public void removeAllByUserId(@NotNull final String id) {
        entityManager.remove(findOneById(id));
    }

    public void removeAll() {
        for (final UserEntity userEntity : findAll()) entityManager.remove(userEntity);
    }

    public void merge(@NotNull final UserEntity userEntity) {
        entityManager.merge(userEntity);
    }

    public List<UserEntity> findAll() {
        return entityManager.createQuery("SELECT u FROM UserEntity u", UserEntity.class).getResultList();
    }

    public UserEntity findByLoginAndPwd(@NotNull final String login, @NotNull final String pwdHash) {
        return entityManager.createQuery("SELECT u FROM UserEntity u WHERE u.login = :login AND u.pwdHash = :pwdHash", UserEntity.class)
                .setParameter("login", login).setParameter("pwdHash", pwdHash).getSingleResult();
    }

}
