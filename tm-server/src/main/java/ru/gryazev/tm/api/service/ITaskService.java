package ru.gryazev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.repository.ITaskRepository;
import ru.gryazev.tm.dto.Task;
import ru.gryazev.tm.entity.TaskEntity;
import ru.gryazev.tm.repository.TaskRepository;
import ru.gryazev.tm.util.CompareUtil;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

public interface ITaskService {

    @Nullable
    public TaskEntity create(@Nullable String userId, @Nullable TaskEntity task);

    @Nullable
    public TaskEntity edit(@Nullable String userId, @Nullable TaskEntity task) throws Exception;

    @Nullable
    public TaskEntity findOne(@Nullable String userId, @Nullable String taskId);

    @NotNull
    public List<TaskEntity> listTaskUnlinked(@Nullable String userId);

    public void remove(@Nullable String userId, @Nullable String taskId);

    public void removeByProjectId(@Nullable String userId, @Nullable String projectId);

    @Nullable
    public String getTaskId(@Nullable String userId, @Nullable String projectId, int taskIndex);

    @NotNull
    public List<TaskEntity> findByProjectId(@Nullable String userId, @Nullable String projectId);

    @NotNull
    public List<TaskEntity> findByName(@Nullable String userId, @Nullable String taskName);

    @NotNull
    public List<TaskEntity> findByDetails(@Nullable String userId, @Nullable String taskDetails);

    @NotNull
    public List<TaskEntity> listTaskByProjectSorted(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String sortType
    );

    @NotNull
    public List<TaskEntity> listTaskUnlinkedSorted(@Nullable String userId, @Nullable String sortType);

    @NotNull
    public List<TaskEntity> findByUserId(@Nullable String userId);

    public void removeAll();

    @NotNull
    public List<TaskEntity> findAll();

}
